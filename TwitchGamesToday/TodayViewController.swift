//
//  TodayViewController.swift
//  TwitchGamesToday
//
//  Created by Gustavo Luís Soré on 10/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit
import NotificationCenter
import SDWebImage

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet var firstGameImageView: UIImageView!
    @IBOutlet var secondGameImageView: UIImageView!
    @IBOutlet var thirdGameImageView: UIImageView!
    
    var game1:Game?
    var game2:Game?
    var game3:Game?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        firstGameImageView.isUserInteractionEnabled = true
        secondGameImageView.isUserInteractionEnabled = true
        thirdGameImageView.isUserInteractionEnabled = true
        
        GameWorker.shared.fetchTopGames(offset: 0) { (result) in
            switch result{
            case .Success(result: let games):
                if games.count >= 3{
                    let urlString1 = games[0].box.medium!
                    let urlString2 = games[1].box.medium!
                    let urlString3 = games[2].box.medium!
                    
                    let url1 = URL.init(string: urlString1)
                    let url2 = URL.init(string: urlString2)
                    let url3 = URL.init(string: urlString3)
                    
                    self.game1 = games[0]
                    self.game2 = games[1]
                    self.game3 = games[2]
                    
                    let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(self.game1Call))
                    tap1.numberOfTapsRequired = 1
                    self.firstGameImageView.addGestureRecognizer(tap1)
                
                    let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(self.game2Call))
                    tap2.numberOfTapsRequired = 1
                    self.secondGameImageView.addGestureRecognizer(tap2)
                    
                    let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(self.game3Call))
                    tap3.numberOfTapsRequired = 1
                    self.thirdGameImageView.addGestureRecognizer(tap3)
                    
                    DispatchQueue.main.async {
                        try! self.firstGameImageView.image = UIImage.init(data: Data.init(contentsOf: url1!))
                        try! self.secondGameImageView.image = UIImage.init(data: Data.init(contentsOf: url2!))
                        try! self.thirdGameImageView.image = UIImage.init(data: Data.init(contentsOf: url3!))
                    }
//                    self.secondGameImageView.sd_setImage(with: url2, completed: nil)
//                    self.thirdGameImageView.sd_setImage(with: url3, completed: nil)
                }
                break
            default:
                break
            }
        }
    }
    
    @objc func game1Call()
    {
        if let game = game1{
            if let url = URL.init(string: "\(Constants.Application.URLScheme)://\(game._id!)"){
                extensionContext?.open(url, completionHandler: nil)
            }
        }
    }
    
    @objc func game2Call()
    {
        if let game = game2{
            if let url = URL.init(string: "\(Constants.Application.URLScheme)://\(game._id!)"){
                extensionContext?.open(url, completionHandler: nil)
            }
        }
    }
    
    @objc func game3Call()
    {
        if let game = game3{
            if let url = URL.init(string: "\(Constants.Application.URLScheme)://\(game._id!)"){
                extensionContext?.open(url, completionHandler: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
