### Desafio iOS

## Introduçao

Link para a descrição do teste:
https://github.com/GrupoZapVivaReal/join_us-ios/blob/senior/README_Senior.md

Contem:

Home - Top Games

	•	Listagem dos jogos ordenados por visualização.
	•	Botão para favoritar jogos nas células.
	•	Barra de busca para filtrar lista de jogos por nome.
	•	Pull-to-refresh para atualizar a lista.
	•	Paginação na lista: Carregar 20 jogos por vez, baixando a próxima página ao chegar no fim da lista.

Detalhes do jogo

	•	Botão de favorito.
	•	Foto em tamanho maior, nome e número de visualizações do jogo.

Favoritos

	•	Listagem dos jogos favoritados pelo usuário.
	•	Favoritos devem ser persistidos para serem acessados offline.
	•	Interface de lista vazia caso o usuário não tenha favoritos.

Requisitos Essenciais

	•	Usar Swift 4.
	•	Interface desenvolvida no Storyboard, utilizando Auto Layout.
	•	Usar UICollectionView com no minimo 2 colunas para representar a lista dos jogos e favoritos.
	•	App Universal: Você deve desenvolver uma interface que se adapte bem em telas maiores.
	•	CoreData: Favoritos devem ser salvos no CoreData para que os usuários consigam acessar offline.
	•	Tratamento de falha de conexão: Avise o usuário quando o download dos jogos falhar por falta de conexão.
	•	Today Widget: Um Widget onde podemos visualizar rapidamente os 3 jogos mais populares do Twitch.
	•	Drag and Drop: Implemente uma interação com Drag and Drop onde podemos selecionar um jogo da lista principal e arrastar para a lista de favoritos, favoritando o jogo.
	•	Testes unitários.
	•	Integração com Fastlane com uma lane para rodar os testes unitários.

Bônus

	•	Acessar os jogos através do iMessage para o compartilhamento. 

## Executar o projeto

Para executar o projeto é necessário:


	• Utilizando o Xcode mais recente (9.4)
	• Git, Fastlane e Cocoapods instalados na máquina
	• Acesso a internet para realização dos testes unitários da API

Passos para executar o projeto (pelo terminal):

	• Ir até o diretório:
		cd twitchgames/

	• Instalar os pods:
		pod install

	• Rodar testes unitários:
		fastlane tests

	• Abrir o projeto no Xcode:
		open TwitchGames.xcworkspace/

	• Escolher um time de desenvolvimento e rodar o projeto no simulador ou dispositivo com iOS 11.

OBS: Caso haja uma falha ao executar os testes unitários via fastlane, abra o projeto escolha um time de desenvolvimento e repita o comando "fastlane tests”.

Qualquer outra dúvida ou problema que queira esclarecer entre em contato em sore.gustavo@gmail.com
