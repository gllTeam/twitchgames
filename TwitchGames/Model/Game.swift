//
//  Game.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 05/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation

struct Game: Equatable,Codable
{
    var name: String?
    var popularity: Int?
    var _id: Int?
    var locale:String?
    
    var box: ImageReference
    
    var favorite:Bool? = false
    
    var viewers:Int?
}

// MARK: - Complemention models

struct ImageReference: Equatable,Codable
{
    var large: String?
    var medium: String?
    var small: String?
}

// MARK: - Parse top models

struct RootTopGame:Equatable,Codable
{
    var top:[RootGame]
}

struct RootGame:Equatable,Codable
{
    var game:Game
    var viewers:Int
}

// MARK: - Parse search models

struct RootSearchGame:Equatable,Codable
{
    var games:[Game]?
}
