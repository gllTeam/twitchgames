//
//  GameWorker.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 05/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation

class GameWorker
{
    var browserGameEngine:GameBrowserProtocol
    var favoriteGameEngine:GameFavoriteProtocol
    
    static var shared:GameWorker = GameWorker(browserEngine: GameBrowserAPI(), favoriteEngine: GameFavoriteCoreData())
    
    init(browserEngine: GameBrowserProtocol,favoriteEngine:GameFavoriteProtocol)
    {
        self.browserGameEngine = browserEngine
        self.favoriteGameEngine = favoriteEngine
    }
    
    func fetchTopGames(offset:Int,completionHandler: @escaping FetchTopGamesCompletionHandler)
    {
        browserGameEngine.fetchTopGames(offset: offset) { (result) in
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    func searchGame(query:String,completionHandler: @escaping SearchGameCompletionHandler)
    {
        browserGameEngine.searchGame(query: query) { (result) in
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    func addGameToFavorite(game:Game,completionHandler: @escaping AddGameToFavoriteCompletionHandler)
    {
        favoriteGameEngine.addGameToFavorite(game: game) { (result) in
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    func fetchFavoriteGames(completionHandler: @escaping FetchFavoriteGamesCompletionHandler)
    {
        favoriteGameEngine.fetchFavoriteGames { (result) in
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
    
    func deleteGameFromFavorite(id:Int,completionHandler: @escaping DeleteGameFromFavoriteCompletionHandler)
    {
        favoriteGameEngine.deleteGameFromFavorite(id: id) { (result) in
            DispatchQueue.main.async {
                completionHandler(result)
            }
        }
    }
}

// MARK: - Protocols

protocol GameBrowserProtocol {
    func fetchTopGames(offset:Int,completionHandler: @escaping FetchTopGamesCompletionHandler)
    func searchGame(query:String,completionHandler: @escaping SearchGameCompletionHandler)
}

protocol GameFavoriteProtocol {
    func addGameToFavorite(game:Game,completionHandler: @escaping AddGameToFavoriteCompletionHandler)
    func fetchFavoriteGames(completionHandler: @escaping FetchFavoriteGamesCompletionHandler)
    func deleteGameFromFavorite(id:Int,completionHandler: @escaping DeleteGameFromFavoriteCompletionHandler)
}

// MARK: - Typealias

typealias FetchTopGamesCompletionHandler = (GameBrowserResult<[Game]>) -> Void
typealias SearchGameCompletionHandler = (GameBrowserResult<[Game]>) -> Void

typealias AddGameToFavoriteCompletionHandler = (GameFavoriteResult<Game>) -> Void
typealias FetchFavoriteGamesCompletionHandler = (GameFavoriteResult<[Game]>) -> Void
typealias DeleteGameFromFavoriteCompletionHandler = (GameFavoriteResult<Game>) -> Void

// MARK: - Results

enum GameBrowserResult<U>
{
    case Success(result: U)
    case Failure(error: GameBrowserError)
}

enum GameFavoriteResult<U>
{
    case Success(result: U)
    case Failure(error: GameFavoriteError)
}

// MARK: - Erros

enum GameBrowserError: Equatable, Error
{
    case CannotFetch(String)
    case NoInternetAcces
    case WrongURLFormat
    case NoData
    case ParseError(String)
    case EmptySerach
}

enum GameFavoriteError: Equatable, Error
{
    case CannotAdd(String)
    case CannotFetch(String)
    case CannotDelete(String)
    case Empty
}
