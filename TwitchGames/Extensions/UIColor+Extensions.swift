//
//  UIColor+Extensions.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 06/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit

extension UIColor
{
    func color(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    var darkPurple:UIColor{
        return color(hex: "#4B387A")
    }
    
    var lightPurple:UIColor{
        return color(hex: "#6445A2")
    }
    
    var darkRed:UIColor{
        return color(hex: "#9e2e2e")
    }
    
    var darkGreen:UIColor{
        return color(hex: "#1b7e1b")
    }
}
