//
//  GameBrowserViewModel.swift
//  TwitchGames
//
//  Created by Gustavo on 06/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GameBrowserViewModel
{
    
    //MARK: - Worker
    
    let gameWork:GameWorker
    
    //MARK: - Rx Properties
    
    var games:BehaviorRelay<[Game]> = BehaviorRelay<[Game]>(value: [])
    var gamesObservable: Observable<[Game]> {
        return self.games.asObservable()
    }
    var errorObservable = PublishSubject<GameBrowserError>()
    let disposeBag = DisposeBag()
    
    //MARK: - Properties
    
    var topGames:[Game] = []
    var searchGames:[Game] = []
    
    private var offset:Int = 0
    private var numberOfGamesPerRequest:Int = 0
    
    private var canDownload:Bool = false
    
    private var isSearching:Bool = false
    
    //MARK: - Methods
    
    init(worker:GameWorker,numberOfGamesPerRequest:Int) {
        gameWork = worker
        self.numberOfGamesPerRequest = numberOfGamesPerRequest
    }
    
    private func increaseOffset()
    {
        offset = offset + numberOfGamesPerRequest
    }
    
    func getMoreGamesIfNedded(currentIndex:Int)
    {
        if isSearching == false{
            if currentIndex >= topGames.count - (numberOfGamesPerRequest / 3){
                if canDownload{
                    getTopGames()
                }
            }
        }
    }
    
    func eraseAll()
    {
        offset = 0
        topGames.removeAll()
    }
    
    func getTopGames()
    {
        canDownload = false
        isSearching = false
        
        gameWork.fetchTopGames(offset: offset) { (result) in
            switch result{
            case .Success(result: let games):
                self.topGames.append(contentsOf: games)
                self.updateFavorite()
                self.increaseOffset()
                break
            case .Failure(error: let error):
                self.errorObservable.onNext(error)
                break
            }
            self.canDownload = true
        }
        
    }
    
    func searchGame(query:String)
    {
        isSearching = true
        
        gameWork.searchGame(query: query) { (result) in
            switch result{
            case .Success(result: let games):
                self.searchGames.append(contentsOf: games)
                self.updateFavorite()
                break
            case .Failure(error: let error):
                self.errorObservable.onNext(error)
                break
            }
        }
    }
    
    func addGameToFavorite(game:Game)
    {
        if let favorite = game.favorite{
            
            if favorite == true{
                
                gameWork.deleteGameFromFavorite(id: game._id!) { (result) in
                    switch result{
                    case .Success(result: var deletedGame):
                        deletedGame.favorite = false
                        self.updateGameInDataSource(gameToUpdate: deletedGame)
                        break
                    default: break
                    }
                }
                
            } else{
                
                gameWork.addGameToFavorite(game: game) { (result) in
                    switch result{
                    case .Success(result: var addedGame):
                        addedGame.favorite = true
                        self.updateGameInDataSource(gameToUpdate: addedGame)
                        break
                    default: break
                    }
                }
                
            }
            
        }
    }
    
    private func updateGameInDataSource(gameToUpdate:Game)
    {
        if isSearching{
            for i in 0...searchGames.count - 1{
                if searchGames[i]._id == gameToUpdate._id{
                    searchGames[i] = gameToUpdate
                    self.updateFavorite()
                    
                }
            }
        } else{
            for i in 0...topGames.count - 1{
                if topGames[i]._id == gameToUpdate._id{
                    topGames[i] = gameToUpdate
                    self.updateFavorite()
                }
            }
        }
    }
    
    func updateFavorite()
    {
        gameWork.fetchFavoriteGames { (result) in
            switch result{
            case .Success(result: let games):
                var currentGames:[Game] = []
                if self.isSearching{
                    currentGames = self.searchGames
                } else{
                    currentGames = self.topGames
                }
                if currentGames.isEmpty{
                    return
                }
                for i in 0...currentGames.count - 1{
                    currentGames[i].favorite = false
                }
                for i in 0...currentGames.count - 1{
                    for game in games{
                        if currentGames[i]._id == game._id{
                            currentGames[i].favorite = true
                        }
                    }
                }
                self.games.accept(currentGames)
                break
            default: break
            }
        }
    }

}
