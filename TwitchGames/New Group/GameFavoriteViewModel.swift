//
//  GameFavoriteViewModel.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 10/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class GameFavoriteViewModel
{
    //MARK: - Worker
    
    let gameWork:GameWorker
    
    //MARK: - Rx Properties
    
    var games:BehaviorRelay<[Game]> = BehaviorRelay<[Game]>(value: [])
    var gamesObservable: Observable<[Game]> {
        return self.games.asObservable()
    }
    var errorObservable = PublishSubject<GameFavoriteError>()
    let disposeBag = DisposeBag()
    
    //MARK: - Properties
    
    var favoriteGames:[Game] = []
    
    //MARK: - Methods
    
    init(worker:GameWorker) {
        gameWork = worker
    }
    
    func getFavoriteGames()
    {
        gameWork.fetchFavoriteGames { (result) in
            switch result{
            case .Success(result: let games):
                self.favoriteGames.removeAll()
                self.favoriteGames.append(contentsOf: games)
                if self.favoriteGames.isEmpty{
                    self.games.accept(self.favoriteGames)
                    return
                }
                for i in 0...self.favoriteGames.count - 1{
                    self.favoriteGames[i].favorite = true
                }
                self.games.accept(self.favoriteGames)
                break
            case .Failure(error: let error):
                self.errorObservable.onNext(error)
                break
            }
        }
    }
    
    func deleteGameFromFavorites(game:Game,completionHandle:@escaping ()->())
    {
        if let id = game._id{
            gameWork.deleteGameFromFavorite(id: id) { (result) in
                switch result{
                case .Success(result: let deletedGame):
                    for i in 0...self.favoriteGames.count - 1{
                        if self.favoriteGames[i]._id == deletedGame._id{
                            self.favoriteGames.remove(at: i)
                            break
                        }
                    }
                    self.games.accept(self.favoriteGames)
                    completionHandle()
                    break
                case .Failure(error: let error):
                    self.errorObservable.onNext(error)
                    completionHandle()
                    break
                }
            }
        }
    }
    
    func insertNewGame(game:Game,index:Int,completion:@escaping (_ success:Bool)->())
    {
        
        for gameAux in favoriteGames{
            if gameAux._id == game._id{
                completion(false)
                return
            }
        }
        
        var gameToAdd = game
        gameToAdd.favorite = true
        
        //favoriteGames.insert(gameToAdd, at: index)
        favoriteGames.insert(gameToAdd, at: 0)
        self.games.accept(favoriteGames)
        
        gameWork.addGameToFavorite(game: gameToAdd) { (result) in
            switch result{
            case .Success(result: _):
                completion(true)
                break
            case .Failure(error: _):
                completion(false)
                break
            }
        }
    }
}
