//
//  FavoriteGamesViewController.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 10/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FavoriteGamesViewController: BaseViewController,UICollectionViewDelegateFlowLayout {
    
    //MARK: - Properties
    
    var viewModel: GameFavoriteViewModel!
    
    let disposeBag = DisposeBag()

    //MARK: - Outlets
    
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupViewModel()
        bindUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if viewModel.games.value.isEmpty{
            showEmptyView(message: Constants.Messages.NoFavoriteResults)
        }
        viewModel.getFavoriteGames()
    }

    //MARK: - Methods
    
    func setupCollectionView(){
        self.collectionView.rx.setDelegate(self)
            .disposed(by: self.disposeBag)
        self.collectionView.dragInteractionEnabled = true
        let nib = UINib(nibName: GameCollectionCell.identifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: GameCollectionCell.identifier)
    }
    
    func setupViewModel(){
        
        let worker = GameWorker.shared
        viewModel = GameFavoriteViewModel(worker: worker)
        
    }
    
    //MARK: - Bind
    
    func bindUI()
    {
        
        self.viewModel.gamesObservable
            .bind(to:
                collectionView.rx
                    .items(cellIdentifier: GameCollectionCell.identifier,
                                        cellType: GameCollectionCell.self)) { (index, game, cell) in
                                        
                                            self.hideEmptyView()
                                            cell.populate(game: game)
                                            cell.addToFavoriteCompletionHandle = { game in
                                                self.viewModel.deleteGameFromFavorites(game: game, completionHandle: {
                                                    if self.viewModel.games.value.isEmpty{
                                                        self.showEmptyView(message: Constants.Messages.NoFavoriteResults)
                                                    }
                                                })
                                            }
                                            
            }.disposed(by: disposeBag)
        
        self.collectionView.rx.modelSelected(Game.self).subscribe { (event) in
            
            if let game = event.element{
                if let vc = ViewControllerFactory.gameDetail(){
                    vc.game = game
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }.disposed(by: disposeBag)

        self.viewModel.errorObservable
            .subscribe(onNext: { (error) in
                
                self.hideLoading()
                self.hideOfflineView()
                
                switch error{
                case .Empty:
                    self.showEmptyView(message: Constants.Messages.NoFavoriteResults)
                    break
                default: break
                }
                
            }, onError: nil, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)
    }

}

extension FavoriteGamesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return GameCollectionCell.size
    }
}
