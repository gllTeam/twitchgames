//
//  ViewController.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 05/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class GameBrowserViewController: BaseViewController , UICollectionViewDelegateFlowLayout{
    
    //MARK: - Properties
    
    var viewModel: GameBrowserViewModel!
    
    let disposeBag = DisposeBag()

    //Drag and drop
    private var favoriteViewController:FavoriteGamesViewController?
    private var favoriteTitleLabel:UILabel?
    private let favoriteTitleLabelHeight:CGFloat = 60.0
    private var gameToAddToFavorite:Game?
    
    //MARK: - Outlets
    @IBOutlet var serachBar: UISearchBar!
    @IBOutlet var collectionView: CustomCollectionView!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupViewModel()
        bindUI()
        
        baseDelegate = self
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.openGameFromToday = {
            self.openGameFromToday()
        }
        
        showLoading()
    }
    
    func openGameFromToday()
    {
        if let gameID = UserDefaults.standard.object(forKey: Constants.Application.GameIDUserDefaults) as? String{
            
            UserDefaults.standard.set(nil, forKey: Constants.Application.GameIDUserDefaults)
            
            for game in viewModel.topGames{
                if "\(game._id!)" == gameID{
                    if let vc = ViewControllerFactory.gameDetail(){
                        vc.game = game
                        self.navigationController?.pushViewController(vc, animated: true)
                        break
                    }
                }
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel.updateFavorite()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.openGameFromToday()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Setups
    
    func setupCollectionView(){
        collectionView.rx.setDelegate(self)
            .disposed(by: self.disposeBag)
        collectionView.loadingDelegate = self
        collectionView.dragInteractionEnabled = true
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        let nib = UINib(nibName: GameCollectionCell.identifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: GameCollectionCell.identifier)
    }
    
    func setupViewModel(){
        
        let worker = GameWorker.shared
        viewModel = GameBrowserViewModel.init(worker: worker, numberOfGamesPerRequest: Constants.GameAPI.kTopGamesLimit)
        
    }

    //MARK: - Bind
    
    func bindUI()
    {
        
        self.viewModel.gamesObservable
            .bind(to:
                collectionView.rx.items(cellIdentifier: GameCollectionCell.identifier,
                                   cellType: GameCollectionCell.self)) { (index, game, cell) in
                                    
                                    self.collectionView.show()
                                    self.hideFeedbackViews()
                                    cell.populate(game: game)
                                    cell.addToFavoriteCompletionHandle = { game in
                                        self.viewModel.addGameToFavorite(game: game)
                                    }
                                    
            }.disposed(by: disposeBag)
        
        self.viewModel.errorObservable
            .subscribe(onNext: { (error) in
                
                self.hideLoading()
                self.hideOfflineView()
                
                switch error{
                case .NoInternetAcces:
                    self.showOfflineNotification()
                    break
                case .EmptySerach:
                    self.collectionView.hide()
                    self.showEmptyView(message: Constants.Messages.NoSearchResults)
                    break
                default: break
                }
                
        }, onError: nil, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)

        
        self.collectionView.rx.modelSelected(Game.self).subscribe { (event) in
            
            if let game = event.element{
                if let vc = ViewControllerFactory.gameDetail(){
                    vc.game = game
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }.disposed(by: disposeBag)
        
        self.serachBar
            .rx.text
            .orEmpty.debounce(0.5, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .filter{!$0.isEmpty}
            .subscribe { (event) in
                
                if let query = event.element{
                    self.showLoading()
                    self.viewModel.searchGame(query: query)
                }
                
        }.disposed(by: disposeBag)
        
        self.serachBar
            .rx.searchButtonClicked
            .subscribe { (event) in
            
            self.serachBar.resignFirstResponder()
            
            }.disposed(by: self.disposeBag)
        
        self.serachBar
            .rx.cancelButtonClicked
            .subscribe { (event) in
                
                self.serachBar.text = ""
                self.serachBar.resignFirstResponder()
                self.showLoading()
                self.viewModel.eraseAll()
                self.viewModel.getTopGames()
                
        }.disposed(by: self.disposeBag)
        
    }
    
    //MARK: - Methods
    
    func hideFeedbackViews()
    {
        hideLoading()
        hideEmptyView()
        hideOfflineView()
    }
    
    func showFavoriteGameViewController(){
        if let favoriteViewController = ViewControllerFactory.gameFavoriteGames(){
            self.favoriteViewController = favoriteViewController
            favoriteViewController.view.frame.origin = CGPoint.init(x: 0, y: view.frame.size.height + favoriteTitleLabelHeight)
            favoriteViewController.collectionView.dropDelegate = self
            
            favoriteTitleLabel = UILabel.init(frame: CGRect.init(x: 0, y: view.frame.size.height, width: view.frame.size.width, height: favoriteTitleLabelHeight))
            favoriteTitleLabel!.backgroundColor = UIColor().lightPurple
            favoriteTitleLabel!.textColor = UIColor.white
            favoriteTitleLabel!.numberOfLines = 0
            favoriteTitleLabel!.font = UIFont.boldSystemFont(ofSize: 20.0)
            favoriteTitleLabel!.text = Constants.Messages.dragAndDrogAddToFavorite
            favoriteTitleLabel!.textAlignment = .center
            
            self.view.addSubview(favoriteViewController.view)
            self.view.addSubview(favoriteTitleLabel!)
            
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 20, initialSpringVelocity: 20, options: UIViewAnimationOptions.curveEaseIn, animations: {
                if let favoriteViewController = self.favoriteViewController{
                    favoriteViewController.view.frame.origin = CGPoint.init(x: 0, y: (self.view.frame.size.height / 2) + self.favoriteTitleLabelHeight)
                }
                if let favoriteTitleLabel = self.favoriteTitleLabel{
                    favoriteTitleLabel.frame.origin = CGPoint.init(x: 0, y: (self.view.frame.size.height / 2))
                }
            }, completion: nil)
        }
    }
    
    func hideFavoriteGameViewController(){
        if let _ = self.favoriteViewController{
            UIView.animate(withDuration: 0.5, animations: {
                if let favoriteViewController = self.favoriteViewController{
                    favoriteViewController.view.frame.origin = CGPoint.init(x: 0, y: self.view.frame.size.height + self.favoriteTitleLabelHeight)
                }
                if let favoriteTitleLabel = self.favoriteTitleLabel{
                    favoriteTitleLabel.frame.origin = CGPoint.init(x: 0, y: self.view.frame.size.height)
                }
            }) { (_) in
                if let favoriteViewController = self.favoriteViewController{
                    favoriteViewController.removeFromParentViewController()
                    self.favoriteViewController = nil
                }
                if let favoriteTitleLabel = self.favoriteTitleLabel{
                    favoriteTitleLabel.removeFromSuperview()
                    self.favoriteTitleLabel = nil
                }
            }
        }
    }

}

//MARK: - Delegates

extension GameBrowserViewController:UICollectionViewDragDelegate
{
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        if self.favoriteViewController == nil{
            let game = viewModel.games.value[indexPath.row]
            let item = DragGameItem.init()
            item.game = game
            self.gameToAddToFavorite = game
            let itemProvider = NSItemProvider(object: item)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = item
            
            showFavoriteGameViewController()
            
            return [dragItem]
        }
        
        return []
        
    }
}

extension GameBrowserViewController:UICollectionViewDropDelegate
{
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath
        {
            destinationIndexPath = indexPath
            self.makeDragAction(indexPath: destinationIndexPath)
        }
        else
        {
            // Get last index path of collection view.
            let section = collectionView.numberOfSections - 1
            let row = collectionView.numberOfItems(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
            self.makeDragAction(indexPath: destinationIndexPath)
        }
    }
    
    private func makeDragAction(indexPath:IndexPath)
    {
        let destinationIndexPath = indexPath
        if let game = self.gameToAddToFavorite{
            self.favoriteViewController?.viewModel.insertNewGame(game: game, index: destinationIndexPath.row, completion: { (success) in
                print(success)
                if success{
                    self.viewModel.updateFavorite()
                    DispatchQueue.main.async {
                        self.showNotification(successNotification: true, message: Constants.Messages.dragAndDrogAddToFavoriteSuccess)
                    }
                } else{
                    DispatchQueue.main.async {
                        self.showNotification(successNotification: false, message: Constants.Messages.dragAndDrogAddToFavoriteFailure)
                    }
                }
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
        self.hideFavoriteGameViewController()
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        
        if collectionView == self.collectionView{
            return UICollectionViewDropProposal(operation: .cancel)
        }
        
        if session.localDragSession != nil
        {
            return UICollectionViewDropProposal(operation: .copy, intent: .insertAtDestinationIndexPath)
        }
        else
        {
            return UICollectionViewDropProposal(operation: .forbidden)
        }

    }
    
}

extension GameBrowserViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return GameCollectionCell.size
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        viewModel.getMoreGamesIfNedded(currentIndex: indexPath.row)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collectionView.updateLoading(contentOffset: scrollView.contentOffset.y)
    }
}

extension GameBrowserViewController:CustomCollectionViewDelegate
{
    func haveToLoading() {
        if self.reachability.connection != .none{
            self.serachBar.text = ""
            self.serachBar.resignFirstResponder()
            self.showLoading()
            viewModel.eraseAll()
            viewModel.getTopGames()
        } else{
            showOfflineNotification()
        }
    }
}

extension GameBrowserViewController:BaseViewControllerDelegate
{
    
    func connectionIsOn() {
        if viewModel.topGames.isEmpty{
            //hideFeedbackViews()
            viewModel.getTopGames()
        } else{
            showOnlineNotification()
        }
    }
    
    func connectionIsOff() {
        if viewModel.topGames.isEmpty{
            hideFeedbackViews()
            showOfflineView()
        }
    }
}
