//
//  GameDetailViewController.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 10/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit

class GameDetailViewController: BaseViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var viewersLabel: UILabel!
    @IBOutlet var favoriteButton: UIBarButtonItem!
    
    var game:Game?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let game = self.game{
            setup(game: game)
        }
    }
    
    private func setup(game:Game)
    {
        if let game = self.game{
            if let favorite = game.favorite{
                if favorite{
                    favoriteButton.tintColor = UIColor.yellow
                }
            }
        }
        
        if let urlString = game.box.large{
            if let url = URL.init(string: urlString){
                imageView.sd_setImage(with: url) { (image, error, chace, url) in
                    if error != nil{
                        if let urlString = self.game?.box.medium{
                            if let url = URL.init(string: urlString){
                                self.imageView.sd_setImage(with: url, completed: nil)
                            }
                        }
                    }
                }
            }
        }
        
        if let name = game.name{
            titleLabel.text = name
        }
        
        if let viewers = game.viewers{
            if viewers > 0{
                viewersLabel.text = "\(Constants.GameDetail.viewersTitle)\(viewers)"
            } else{
                if let popularity = game.popularity{
                    viewersLabel.text = "\(Constants.GameDetail.popularityTitle)\(popularity)"
                }
            }
        }
    
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        if let game = self.game{
            if let favorite = game.favorite{
                
                if favorite{
                    GameWorker.shared.deleteGameFromFavorite(id: game._id!) { (result) in
                        switch result{
                        case .Success(result: var game):
                            game.favorite = false
                            self.game = game
                            self.favoriteButton.tintColor = UIColor.black
                            break
                        default: break
                        }
                    }
                } else{
                    GameWorker.shared.addGameToFavorite(game: game) { (result) in
                        switch result{
                        case .Success(result: var game):
                            game.favorite = true
                            self.game = game
                            self.favoriteButton.tintColor = UIColor.yellow
                            break
                        default: break
                        }
                    }
                }
                
            }
        }
    }
    
}
