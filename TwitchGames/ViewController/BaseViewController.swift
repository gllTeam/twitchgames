//
//  BaseViewController.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 09/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit
import Lottie
import Reachability

@objc protocol BaseViewControllerDelegate:class {
    @objc optional func connectionIsOn()
    @objc optional func connectionIsOff()
}

class BaseViewController: UIViewController {
    
    private var loadingViewAnimation:LOTAnimationView!
    private var loadingView:UIView!
    
    private var offlineViewAnimation:LOTAnimationView!
    private var offlineView:UIView!
    
    private var emptyViewAnimation:LOTAnimationView!
    private var emptyView:UIView!
    private var emptyLabel:UILabel!
    
    private var isShowingOfflineNotification:Bool = false
    
    private var navigationBarHeight:CGFloat!
    
    var baseDelegate:BaseViewControllerDelegate?
    var reachability:Reachability!

    override func viewDidLoad() {
        super.viewDidLoad()
        startMonitoringConection()
        if var navigationBarHeight = navigationController?.navigationBar.frame.height{
            self.navigationBarHeight = navigationBarHeight + 20.0
        } else{
            navigationBarHeight = 60 + 20.0
        }
        
        //Loading
        loadingViewAnimation = LOTAnimationView(name: Constants.Animations.kLoading, bundle: Bundle.main)
        let loadingWidth = view.frame.width / 3
        loadingViewAnimation.frame = CGRect(x: (view.frame.width / 2) - loadingWidth / 2,
                                   y: (view.frame.height / 2) - loadingWidth / 2,
                                   width: loadingWidth,
                                   height: loadingWidth)
        loadingViewAnimation.loopAnimation = true
        
        loadingView = UIView.init(frame: view.frame)
        loadingView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        loadingView.isHidden = true
        loadingView.alpha = 0.0
        loadingView.addSubview(loadingViewAnimation)
        
        view.addSubview(loadingView)
        
        //Offline View
        
        let offlineViewWidth = view.frame.width * 0.7
        let labelHeight:CGFloat = 100
        
        offlineView = UIView.init(frame: CGRect(x: (view.frame.width / 2) - offlineViewWidth / 2,
                                                y: (view.frame.height / 2) - offlineViewWidth / 2 - 100,
                                                width: offlineViewWidth,
                                                height: offlineViewWidth + labelHeight))
        offlineView.backgroundColor = .clear
        
        offlineView.isHidden = true
        offlineView.alpha = 0.0
        
        view.addSubview(offlineView)
        
        //Offline View Animation
        
        offlineViewAnimation = LOTAnimationView(name: Constants.Animations.kOffline, bundle: Bundle.main)

        offlineViewAnimation.frame = CGRect(x: (offlineView.frame.width / 2) - offlineViewWidth / 2,
                                            y: (offlineView.frame.height / 2) - offlineViewWidth / 2,
                                            width: offlineViewWidth,
                                            height: offlineViewWidth)
        offlineViewAnimation.loopAnimation = true
        offlineView.addSubview(offlineViewAnimation)
        
        //Offline View Label
        
        let offlineViewLabel = UILabel(frame: CGRect(x: (offlineView.frame.width / 2) - offlineViewWidth / 2,
                                                     y: offlineViewWidth,
                                                     width: offlineViewWidth,
                                                     height: labelHeight))
        offlineViewLabel.textColor = UIColor().lightPurple
        offlineViewLabel.text = Constants.Messages.NoConnection
        offlineViewLabel.textAlignment = .center
        offlineViewLabel.font = UIFont.systemFont(ofSize: 18.0)
        offlineViewLabel.numberOfLines = 0
        
        offlineView.addSubview(offlineViewLabel)
        
        //Empty View
        
        let emptyViewWidth = view.frame.width / 2
        
        emptyView = UIView(frame: CGRect(x: (view.frame.width / 2) - emptyViewWidth / 2,
                                         y: ((view.frame.height / 2) - emptyViewWidth / 2) - 100,
                                         width: emptyViewWidth,
                                         height: emptyViewWidth + 100))
        
        let emptyViewAnimationWidth = emptyView.frame.width
        
        emptyViewAnimation = LOTAnimationView(name: Constants.Animations.kEmpty, bundle: Bundle.main)
        emptyViewAnimation.frame = CGRect(x: (emptyView.frame.width / 2) - emptyViewAnimationWidth / 2,
                                          y: (emptyView.frame.height / 2) - emptyViewAnimationWidth / 2,
                                          width: emptyViewAnimationWidth,
                                          height: emptyViewAnimationWidth)
        emptyViewAnimation.contentMode = .scaleAspectFit
        emptyViewAnimation.loopAnimation = true
        
        emptyLabel = UILabel(frame: CGRect(x: 0,
                                               y: emptyViewAnimationWidth,
                                               width: emptyViewWidth,
                                               height: 60))
        emptyLabel.textColor = UIColor().lightPurple
        emptyLabel.textAlignment = .center
        emptyLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
        emptyLabel.numberOfLines = 0
        
        emptyView.addSubview(emptyViewAnimation)
        emptyView.addSubview(emptyLabel)
        emptyView.backgroundColor = .clear
        emptyView.isHidden = true
        emptyView.alpha = 0.0
        
        view.addSubview(emptyView)

    }

    private func startMonitoringConection()
    {
        reachability = Reachability()!
        
        reachability.whenReachable = { reachability in
            self.baseDelegate?.connectionIsOn?()
            self.startReachabilityNotifier()
        }
        reachability.whenUnreachable = { _ in
            self.baseDelegate?.connectionIsOff?()
            self.startReachabilityNotifier()
        }
        
        startReachabilityNotifier()
    }
    
    private func startReachabilityNotifier()
    {
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    //MARK: - Loading view methods
    func showLoading()
    {
        if loadingView.isHidden{
            loadingView.isHidden = false
            loadingViewAnimation.play()
            UIView.animate(withDuration: 0.3) {
                self.loadingView.alpha = 1.0
            }
        }
    }
    
    func hideLoading(){
        if loadingView.isHidden == false{
            UIView.animate(withDuration: 0.3, animations: {
                self.loadingView.alpha = 0.0
            }) { (_) in
                self.loadingView.isHidden = true
                self.loadingViewAnimation.stop()
            }
        }
    }
    
    //MARK: - Offline view methods
    
    func showOfflineView()
    {
        if offlineView.isHidden{
            offlineView.isHidden = false
            offlineViewAnimation.play()
            UIView.animate(withDuration: 0.3) {
                self.offlineView.alpha = 1.0
            }
        }
    }
    
    func hideOfflineView(){
        if offlineView.isHidden == false{
            UIView.animate(withDuration: 0.3, animations: {
                self.offlineView.alpha = 0.0
            }) { (_) in
                self.offlineView.isHidden = true
                self.offlineViewAnimation.stop()
            }
        }
    }
    
    //MARK: - Top empty view methods
    
    func showEmptyView(message:String)
    {
        if emptyView.isHidden{
            emptyLabel.text = message
            emptyView.isHidden = false
            emptyViewAnimation.play()
            UIView.animate(withDuration: 0.3) {
                self.emptyView.alpha = 1.0
            }
        }
    }
    
    func hideEmptyView(){
        if emptyView.isHidden == false{
            UIView.animate(withDuration: 0.3, animations: {
                self.emptyView.alpha = 0.0
            }) { (_) in
                self.emptyView.isHidden = true
                self.emptyViewAnimation.stop()
            }
        }
    }
    
    //MARK: - Top notifications methods
    
    func showOfflineNotification()
    {
        if isShowingOfflineNotification == false{
            isShowingOfflineNotification = true
            let offlineNotificationLabelHeight:CGFloat = 50
            let offlineNotificationLabel = UILabel(frame: CGRect(x: 0, y: 0 - offlineNotificationLabelHeight, width: view.frame.width, height: offlineNotificationLabelHeight))
            offlineNotificationLabel.backgroundColor = UIColor().darkRed
            offlineNotificationLabel.textColor = UIColor.white
            offlineNotificationLabel.textAlignment = .center
            offlineNotificationLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
            offlineNotificationLabel.text = Constants.Messages.NoConnectionNotification
            
            view.addSubview(offlineNotificationLabel)
            
            UIView.animate(withDuration: 0.5, animations: {
                offlineNotificationLabel.frame.origin.y = self.navigationBarHeight
            }) { (_) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                    UIView.animate(withDuration: 0.5, animations: {
                        offlineNotificationLabel.frame.origin.y = 0 - offlineNotificationLabelHeight
                    }, completion: { (_) in
                        offlineNotificationLabel.removeFromSuperview()
                        self.isShowingOfflineNotification = false
                    })
                }
            }
        }
    }
    
    func showOnlineNotification()
    {
        let onlineNotificationLabelHeight:CGFloat = 50
        let onlineNotificationLabel = UILabel(frame: CGRect(x: 0, y: 0 - onlineNotificationLabelHeight, width: view.frame.width, height: onlineNotificationLabelHeight))
        onlineNotificationLabel.backgroundColor = UIColor().darkGreen
        onlineNotificationLabel.textColor = UIColor.white
        onlineNotificationLabel.textAlignment = .center
        onlineNotificationLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        onlineNotificationLabel.text = Constants.Messages.ConnectionNotification
        
        view.addSubview(onlineNotificationLabel)
        
        UIView.animate(withDuration: 0.5, animations: {
            onlineNotificationLabel.frame.origin.y = self.navigationBarHeight
        }) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                UIView.animate(withDuration: 0.5, animations: {
                    onlineNotificationLabel.frame.origin.y = 0 - onlineNotificationLabelHeight
                }, completion: { (_) in
                    onlineNotificationLabel.removeFromSuperview()
                })
            }
        }
    }
    
    func showNotification(successNotification:Bool,message:String)
    {
        let onlineNotificationLabelHeight:CGFloat = 50
        let onlineNotificationLabel = UILabel(frame: CGRect(x: 0, y: 0 - onlineNotificationLabelHeight, width: view.frame.width, height: onlineNotificationLabelHeight))
        if successNotification{
            onlineNotificationLabel.backgroundColor = UIColor().darkGreen
        } else{
            onlineNotificationLabel.backgroundColor = UIColor().darkRed
        }
        onlineNotificationLabel.textColor = UIColor.white
        onlineNotificationLabel.textAlignment = .center
        onlineNotificationLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        onlineNotificationLabel.text = message
        
        view.addSubview(onlineNotificationLabel)
        
        UIView.animate(withDuration: 0.5, animations: {
            onlineNotificationLabel.frame.origin.y = self.navigationBarHeight
        }) { (_) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                UIView.animate(withDuration: 0.5, animations: {
                    onlineNotificationLabel.frame.origin.y = 0 - onlineNotificationLabelHeight
                }, completion: { (_) in
                    onlineNotificationLabel.removeFromSuperview()
                })
            }
        }
    }
}
