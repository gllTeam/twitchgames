//
//  CustomCollectionView.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 09/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit
import Lottie

protocol CustomCollectionViewDelegate:class {
    func haveToLoading()
}

class CustomCollectionView: UICollectionView {
    
    var loadingView:LOTAnimationView?
    
    var loadingDelegate:CustomCollectionViewDelegate?
    
    private var isLoaing:Bool = false

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        addTopLoading()
    }
    
    private func addTopLoading()
    {
        let width = self.frame.width / 4
        let size = CGSize(width: width, height: width)
        let origin = CGPoint(x: (self.frame.width / 2) - (width / 2), y: 0 - width)
        loadingView = LOTAnimationView(name: Constants.Animations.kLoading, bundle: Bundle.main)
        loadingView!.frame = CGRect(x: origin.x, y: origin.y, width: size.width, height: size.height)
        self.addSubview(loadingView!)
    }
    
    func hideLoading()
    {
        UIView.animate(withDuration: 0.7, animations: {
            self.contentOffset = CGPoint(x: self.contentOffset.x, y: 0)
        }) { (_) in
            self.isLoaing = false
            self.isScrollEnabled = true
        }
    }
    
    func hide()
    {
        if self.isHidden == false{
            UIView.animate(withDuration: 0.5, animations: {
                self.alpha = 0.0
            }) { (_) in
                self.isHidden = true
            }
        }
    }
    
    func show()
    {
        if self.isHidden{
            self.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.alpha = 1.0
            }
        }
    }
    
    func updateLoading(contentOffset:CGFloat)
    {
        if isLoaing == false{
            if let loadingView = self.loadingView{
                if contentOffset < 0{
                    let percent = (contentOffset * -1) / loadingView.frame.width
                    if percent > 1.0{
                        loadingView.loopAnimation = true
                        loadingView.play()
                        hideLoading()
                        loadingDelegate?.haveToLoading()
                        isLoaing = true
                        self.isScrollEnabled = false
                    } else{
                        loadingView.animationProgress = percent
                    }
                }
            }
        }
    }

}
