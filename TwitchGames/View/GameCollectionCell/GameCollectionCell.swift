//
//  GameCollectionCell.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 07/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit
import SDWebImage

class GameCollectionCell: UICollectionViewCell {

    @IBOutlet var favoriteButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    private var isFavorite:Bool = false
    private var currentGame:Game?
    
    var addToFavoriteCompletionHandle:((_ game: Game) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.contentMode = .scaleAspectFit
        
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        self.titleLabel.backgroundColor = UIColor().lightPurple
    }
    
    static var identifier: String {
        return "GameCollectionCell"
    }
    
    static var size: CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            return CGSize(width: 135, height: 189)
        }
        
        let width:CGFloat = (UIWindow().bounds.width / 2) - 15
        let height:CGFloat = width * 1.4
        
        return CGSize(width: width, height: height)
    }
    
    func populate(game:Game)
    {
        currentGame = game
        
        if let urlString = game.box.medium{
            if let url = URL.init(string: urlString){
                imageView.sd_setImage(with: url, completed: nil)
            }
        }
        
        if let name = game.name{
            titleLabel.text = name
            titleLabel.backgroundColor = UIColor().lightPurple
        }
        
        if let favorite = game.favorite{
            isFavorite = favorite
            if isFavorite{
                favoriteButton.tintColor = UIColor.yellow
            } else{
                favoriteButton.tintColor = UIColor.white
            }
        }
        
    }

    @IBAction func favoriteAction(_ sender: Any) {
        if let game = currentGame{
            
            addToFavoriteCompletionHandle?(game)
            
        }
    }
    
}
