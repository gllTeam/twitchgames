//
//  Constants.swift
//  TwitchGames
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation

struct Constants {
    
    struct GameAPI {
        
        //Header
        static let kClient_ID_KEY:String = "Client-ID"
        static let kClient_ID:String = "8vg99vv9e3n5gch9oh5m482w69v3dv"
        
        static let kAccept_KEY:String = "Accept"
        static let kAccept:String = "application/vnd.twitchtv.v5+json"
        
        //Top Games
        static let kTopGamesLimit:Int = 20
        static let kTopGamesURLString:String = "https://api.twitch.tv/kraken/games/top?limit=\(kTopGamesLimit)&offset="
        
        //Search Game
        static let kSearchGameURLString:String = "https://api.twitch.tv/kraken/search/games?query="
        
    }
    
    struct GameCoreData {
        
        static let kResource = "Game"
        static let kResource_Extension = "momd"
        
        static let kDataBase_Name = "GameFavorite.sqlite"
        
        static let kEntity_Name = "ManagedGame"
        
    }
    
    struct Animations {
        
        static let kLoading = "loading"
        static let kEmpty = "empty"
        static let kOffline = "offline"
        
    }
    
    struct Messages {
        
        static let NoConnection = "Sem conexão\nAtualizara automaticamente quando voltar a conexão"
        static let NoConnectionNotification = "Offline"
        static let ConnectionNotification = "Online"
        
        static let NoSearchResults = "Nenhum resultado para a busca"
        
        static let NoFavoriteResults = "Nenhum game favoritado"
        
        static let dragAndDrogAddToFavorite = "Solte aqui para adicionar aos favoritos"
        static let dragAndDrogAddToFavoriteSuccess = "Adicionado com sucesso"
        static let dragAndDrogAddToFavoriteFailure = "Game já adicionado aos favoritos"
    }
    
    struct GameDetail{
        
        static let viewersTitle = "Numero de visualizações: "
        static let popularityTitle = "Popularidade: "
        
    }
    
    struct Storyboard {
        
        static let StoryBoardIdentifier = "Main"
        static let GameDetailStoryBoardIdentifier = "GameDetailViewController"
        static let FavoriteGamesStoryBoardIdentifier = "FavoriteGamesViewController"
        
    }
    
    struct Application {
        
        static let URLScheme = "twitchgame"
        static let GameIDUserDefaults = "twitchgame"
        static let OpenGameFromTodayNotificationName = "OpenGameFromTodayNotificationName"
        
    }
    
}
