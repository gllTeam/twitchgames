//
//  GameFavoriteCoreData.swift
//  TwitchGames
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import CoreData

class GameFavoriteCoreData: GameFavoriteProtocol
{
    
    //MARK: - Properties
    
    private var mainManagedObjectContext: NSManagedObjectContext
    private var privateManagedObjectContext: NSManagedObjectContext
    
    //MARK: - Lifecycle
    
    init()
    {
        guard let modelURL = Bundle.main.url(forResource: Constants.GameCoreData.kResource, withExtension: Constants.GameCoreData.kResource_Extension) else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        mainManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        mainManagedObjectContext.persistentStoreCoordinator = psc
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsURL = urls[urls.endIndex-1]

        let storeURL = documentsURL.appendingPathComponent(Constants.GameCoreData.kDataBase_Name)
        do {
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
        
        privateManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateManagedObjectContext.parent = mainManagedObjectContext
    }
    
    deinit
    {
        do {
            try self.mainManagedObjectContext.save()
        } catch {
            fatalError("Error deinitializing main managed object context")
        }
    }
    
    //MARK: - CRUD
    
    func addGameToFavorite(game: Game, completionHandler: @escaping AddGameToFavoriteCompletionHandler) {
        privateManagedObjectContext.perform {
            do {
                let managedGame = NSEntityDescription.insertNewObject(forEntityName: Constants.GameCoreData.kEntity_Name, into: self.privateManagedObjectContext) as! ManagedGame
                managedGame.from(game: game)
                try self.privateManagedObjectContext.save()
                completionHandler(GameFavoriteResult.Success(result: game))
                try self.mainManagedObjectContext.save()
            } catch {
                completionHandler(GameFavoriteResult.Failure(error: GameFavoriteError.CannotAdd("CoreData error: \(error.localizedDescription)")))
            }
            
        }
    }
    
    func fetchFavoriteGames(completionHandler: @escaping FetchFavoriteGamesCompletionHandler) {
        privateManagedObjectContext.perform {
            do {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.GameCoreData.kEntity_Name)
                let results = try self.privateManagedObjectContext.fetch(fetchRequest) as! [ManagedGame]
                let games = results.map { $0.toGame() }
                completionHandler(GameFavoriteResult.Success(result: games))
            } catch {
                completionHandler(GameFavoriteResult.Failure(error: GameFavoriteError.CannotFetch("CoreData error: \(error.localizedDescription)")))
            }
            
        }
    }
    
    func deleteGameFromFavorite(id: Int, completionHandler: @escaping DeleteGameFromFavoriteCompletionHandler) {
        privateManagedObjectContext.perform {
            do {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.GameCoreData.kEntity_Name)
                fetchRequest.predicate = NSPredicate(format: "id == %d", id)
                let results = try self.privateManagedObjectContext.fetch(fetchRequest) as! [ManagedGame]
                if let managedGame = results.first {
                    let game = managedGame.toGame()
                    self.privateManagedObjectContext.delete(managedGame)
                    do {
                        try self.privateManagedObjectContext.save()
                        try self.mainManagedObjectContext.save()
                        completionHandler(GameFavoriteResult.Success(result: game))
                    } catch {
                        completionHandler(GameFavoriteResult.Failure(error: GameFavoriteError.CannotDelete("Cannot delete game with id \(id)")))
                    }
                } else {
                    completionHandler(GameFavoriteResult.Failure(error: GameFavoriteError.CannotDelete("Cannot fetch game with id \(id) to delete")))
                }
            } catch {
                completionHandler(GameFavoriteResult.Failure(error: GameFavoriteError.CannotDelete("Cannot fetch game with id \(id) to delete")))
            }
        }
    }
    
}
