//
//  ManagedGame.swift
//  TwitchGames
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation
import CoreData

extension ManagedGame
{
    func toGame() -> Game
    {
        let name:String = self.name ?? ""
        let popularity:Int = Int(self.popularity)
        let _id:Int = Int(self.id)
        let locale:String = self.locale ?? ""
        
        let large_image = self.large_image ?? ""
        let medium_image = self.medium_image ?? ""
        let small_image = self.small_image ?? ""
        
        let favorite = self.favorite
        
        let viewers = Int(self.viewers)
        
        let imageReference = ImageReference.init(large: large_image, medium: medium_image, small: small_image)
        
        return Game(name: name, popularity: popularity, _id: _id, locale: locale, box: imageReference,favorite: favorite,viewers: viewers)
    }
    
    func from(game: Game)
    {
        self.name = game.name ?? ""
        self.popularity = Int32(game.popularity ?? 0)
        self.id = Int64(game._id ?? 0)
        self.locale = game.locale ?? ""
        
        self.large_image = game.box.large ?? ""
        self.medium_image = game.box.medium ?? ""
        self.small_image = game.box.small ?? ""
        
        self.favorite = game.favorite ?? false
        
        self.viewers = Int32(game.viewers ?? 0)
    }
}
