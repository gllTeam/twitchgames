//
//  GameBrowserAPI.swift
//  TwitchGames
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation
import Alamofire

class GameBrowserAPI: GameBrowserProtocol
{
    private var requestHeader:[String:String] = [Constants.GameAPI.kClient_ID_KEY:Constants.GameAPI.kClient_ID,
                                            Constants.GameAPI.kAccept_KEY:Constants.GameAPI.kAccept]
    
    func fetchTopGames(offset:Int,completionHandler: @escaping FetchTopGamesCompletionHandler) {
        if Connection.haveInternetAcces{
            guard let url = URL.init(string: "\(Constants.GameAPI.kTopGamesURLString)\(offset)") else{
                completionHandler(.Failure(error: .WrongURLFormat))
                return
            }
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.requestHeader).response { (response) in
                if response.error != nil{
                    completionHandler(.Failure(error: .CannotFetch(response.error.debugDescription)))
                } else{
                    guard let data = response.data else{
                        completionHandler(.Failure(error: .NoData))
                        return
                    }
                    if let res = response.response{
                        if res.statusCode != 200{
                            completionHandler(.Failure(error: GameBrowserError.CannotFetch("HTTP Error Code: \(res.statusCode)")))
                            return
                        }
                    }
                    do{
                        let gamesRoot = try JSONDecoder().decode(RootTopGame.self, from: data)
                        var games:[Game] = []
                        for var object in gamesRoot.top{
                            object.game.favorite = false
                            object.game.viewers = object.viewers
                            games.append(object.game)
                        }
                        completionHandler(.Success(result: games))
                    } catch{
                        completionHandler(.Failure(error: .ParseError(error.localizedDescription)))
                    }
                }
            }
        } else {
            completionHandler(.Failure(error: .NoInternetAcces))
        }
    }
    
    func searchGame(query: String, completionHandler: @escaping SearchGameCompletionHandler) {
        if Connection.haveInternetAcces{
            guard let url = URL.init(string: "\(Constants.GameAPI.kSearchGameURLString)\(query)") else{
                completionHandler(.Failure(error: .WrongURLFormat))
                return
            }
            Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: self.requestHeader).response { (response) in
                if response.error != nil{
                    completionHandler(.Failure(error: .CannotFetch(response.error.debugDescription)))
                } else{
                    guard let data = response.data else{
                        completionHandler(.Failure(error: .NoData))
                        return
                    }
                    if let res = response.response{
                        if res.statusCode != 200{
                            completionHandler(.Failure(error: GameBrowserError.CannotFetch("HTTP Error Code: \(res.statusCode)")))
                            return
                        }
                    }
                    do{
                        let gamesRoot = try JSONDecoder().decode(RootSearchGame.self, from: data)
                        if gamesRoot.games != nil{
                            var games:[Game] = []
                            for var object in gamesRoot.games!{
                                object.favorite = false
                                games.append(object)
                            }
                            completionHandler(.Success(result: games))
                        } else{
                            completionHandler(.Failure(error: .EmptySerach))
                        }
                    } catch{
                        completionHandler(.Failure(error: .ParseError(error.localizedDescription)))
                    }
                }
            }
        } else {
            completionHandler(.Failure(error: .NoInternetAcces))
        }
    }
    
}
