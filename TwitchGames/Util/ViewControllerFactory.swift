//
//  ViewControllerFactory.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 10/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation
import UIKit

class ViewControllerFactory
{
    static func gameDetail() -> GameDetailViewController?
    {
        let storyboard = UIStoryboard(name: Constants.Storyboard.StoryBoardIdentifier, bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: Constants.Storyboard.GameDetailStoryBoardIdentifier) as? GameDetailViewController{
            return controller
        }
        return nil
    }
    
    static func gameFavoriteGames() -> FavoriteGamesViewController?
    {
        let storyboard = UIStoryboard(name: Constants.Storyboard.StoryBoardIdentifier, bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: Constants.Storyboard.FavoriteGamesStoryBoardIdentifier) as? FavoriteGamesViewController{
            return controller
        }
        return nil
    }
}
