//
//  DragGameItem.swift
//  TwitchGames
//
//  Created by Gustavo Luís Soré on 11/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

import Foundation

class DragGameItem:NSObject,NSItemProviderWriting
{
    static var writableTypeIdentifiersForItemProvider: [String] = ["DragGameItem"]
    
    var game:Game?
    
    func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
        
        return nil
        
    }
    
}
