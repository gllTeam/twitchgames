//
//  Connection.swift
//  TwitchGames
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import Reachability

class Connection
{
    static var haveInternetAcces:Bool{
        if let reachability = Reachability(){
            if reachability.connection == .none{
                return false
            }
            return true
        }
        return false
    }
}
