//
//  GameBrowserAPITests.swift
//  TwitchGamesTests
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

@testable import TwitchGames
import XCTest

class GameBrowserAPITests: XCTestCase
{
    // MARK: - Subject under test
    
    var sut: GameBrowserAPI!
    
    // MARK: - Lifecycle
    
    override func setUp()
    {
        sut = GameBrowserAPI()
        super.setUp()
    }
    
    override func tearDown()
    {
        sut = nil
        super.tearDown()
    }
    
    //MARK: - Tests
    
    func testFetchTopGamesShouldFetchGames()
    {
        //When
        
        var fetchGames:[Game]?
        let fetchTopGamesExpectation = expectation(description: "Wait for fetchTopGames() to return")
        sut.fetchTopGames(offset: 0) { (result) in
            switch result{
            case .Success(let games):
                fetchGames = games
                fetchTopGamesExpectation.fulfill()
                break
            case .Failure(let browserError):
                switch browserError{
                case .NoInternetAcces:
                    XCTFail("fetchTopGames() needs internet access")
                    break
                default:
                    XCTFail("fetchTopGames() return an error: \(browserError)")
                    break
                }
            }
        }
        waitForExpectations(timeout: 10.0)
        
        //Then
        
        XCTAssertNotNil(fetchGames, "fetchTopGames() should return a list of games")
        if let fetchGames = fetchGames{
            XCTAssert(fetchGames.count > 0, "fetchTopGames() should return more than zero games")
        }
        
    }
    
    func testSearchGameShouldReturnGames()
    {
        //When
        
        var searchGames:[Game]?
        let searchGameExpectation = expectation(description: "Wait for searchGame() to return")
        sut.searchGame(query: "a") { (result) in
            switch result{
            case .Success(let games):
                searchGames = games
                searchGameExpectation.fulfill()
                break
            case .Failure(let browserError):
                switch browserError{
                case .NoInternetAcces:
                    XCTFail("searchGame() needs internet access")
                    break
                default:
                    XCTFail("searchGame() return an error: \(browserError)")
                    break
                }
            }
        }
        waitForExpectations(timeout: 10.0)
        
        //Then
        
        XCTAssertNotNil(searchGames, "searchGame() should return a list of games")
        if let searchGames = searchGames{
            XCTAssert(searchGames.count > 0, "searchGame() should return more than zero games")
        }
    }
}
