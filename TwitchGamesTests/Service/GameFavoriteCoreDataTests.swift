//
//  GameFavoriteCoreDataTests.swift
//  TwitchGamesTests
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

@testable import TwitchGames
import XCTest

class GameFavoriteCoreDataTests: XCTestCase
{
    // MARK: - Subject under test
    
    var sut: GameFavoriteCoreData!
    var testGames: [Game]!
    
    // MARK: - Lifecycle
    
    override func setUp()
    {
        setupGameFavoriteCoreData()
        super.setUp()
    }
    
    override func tearDown()
    {
        deleteAllGamesInGameFavoriteCoreData()
        sut = nil
        super.tearDown()
    }
    
    // MARK: - Setup
    
    func setupGameFavoriteCoreData()
    {
        sut = GameFavoriteCoreData()
        
        deleteAllGamesInGameFavoriteCoreData()
        
        testGames = [Seeds.Games.zelda, Seeds.Games.tibia]
        
        for game in testGames {
            let expect = expectation(description: "Wait for addGameToFavorite() to return")
            sut.addGameToFavorite(game: game) { (result) in
                switch result{
                case .Success(result: _):
                    expect.fulfill()
                    break
                case .Failure(error: let error):
                    XCTFail("Error in addGameToFavorite(), error: \(error.localizedDescription)")
                    break
                }
            }
            waitForExpectations(timeout: 1.0)
        }
    }
    
    func deleteAllGamesInGameFavoriteCoreData()
    {
        var allGames = [Game]()
        let fetchGamesExpectation = expectation(description: "Wait for fetchFavoriteGames() to return")
        sut.fetchFavoriteGames { (result) in
            switch result{
            case .Success(result: let games):
                allGames = games
                fetchGamesExpectation.fulfill()
                break
            case .Failure(error: let error):
                XCTFail("Error in fetchFavoriteGames(), error: \(error.localizedDescription)")
                break
            }
        }
        waitForExpectations(timeout: 1.0)
        
        for game in allGames {
            let deleteGamesExpectation = expectation(description: "Wait for deletGameFromeFavorite() to return")
            if let id = game._id{
                self.sut.deleteGameFromFavorite(id: id) { (result) in
                    switch result{
                    case .Success(result: _):
                        deleteGamesExpectation.fulfill()
                        break
                    case .Failure(let error):
                        XCTFail("Error in deletGameFromeFavorite(), error: \(error.localizedDescription)")
                        break
                    }
                }
                waitForExpectations(timeout: 1.0)
            } else{
                XCTFail("Error in Game Model, id cant be nil")
            }
        }
    }
    
    //MARK: - Tests
    
    func testFetchFavoriteGamesShouldReturnListOfGames()
    {
        // Given
        
        // When
        var fetchedGames = [Game]()
        var fetchGamesError: GameFavoriteError?
        let expect = expectation(description: "Wait for fetchFavoriteGames() to return")
        sut.fetchFavoriteGames { (result) in
            switch result{
            case .Success(result: let games):
                fetchedGames = games
                break
            case .Failure(error: let error):
                fetchGamesError = error
                break
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 1.0)
        
        // Then
        XCTAssertEqual(fetchedGames.count, testGames.count, "fetchFavoriteGames() should return a list of games")
        for game in fetchedGames {
            XCTAssert(testGames.contains(game), "Fetched games should match the games in the data store")
        }
        XCTAssertNil(fetchGamesError, "fetchFavoriteGames() should not return an error")
    }
    
    func testAddGameShouldCreateAddGame()
    {
        // Given
        let gameToAdd = Seeds.Games.gta
        
        // When
        var addedGame: Game?
        var addedGameError: GameFavoriteError?
        let addGameExpectation = expectation(description: "Wait for addGameToFavorite() to return")
        sut.addGameToFavorite(game: gameToAdd) { (result) in
            switch result{
            case .Success(result: let game):
                addedGame = game
                break
            case .Failure(error: let error):
                addedGameError = error
                break
            }
            addGameExpectation.fulfill()
        }
        waitForExpectations(timeout: 1.0)
        
        // Then
        XCTAssertEqual(addedGame, gameToAdd, "addGameToFavorite() should create a new game")
        XCTAssertNil(addedGameError, "addGameToFavorite() should not return an error")
    }
    
    func testDeletGameFromeFavoriteShouldDeleteExistingGame()
    {
        // Given
        guard let gameToDelete = testGames.first else{
            XCTFail("TestGames must have at least one game for test deletGameFromeFavorite()")
            return
        }
        guard let id = gameToDelete._id else{
            XCTFail("Error in Game Model, id cant be nil for test deletGameFromeFavorite()")
            return
        }
        
        // When
        var deletedGame: Game?
        var deletedGameError: GameFavoriteError?
        let deleteGameExpectation = expectation(description: "Wait for deletGameFromeFavorite() to return")
        sut.deleteGameFromFavorite(id: id) { (result) in
            switch result{
            case .Success(result: let game):
                deletedGame = game
                break
            case .Failure(error: let error):
                deletedGameError = error
                break
            }
            deleteGameExpectation.fulfill()
        }
        waitForExpectations(timeout: 1.0)
        
        // Then
        XCTAssertEqual(deletedGame, gameToDelete, "deletGameFromeFavorite() should delete an existing game")
        XCTAssertNil(deletedGameError, "deletGameFromeFavorite() should not return an error")
    }
}
