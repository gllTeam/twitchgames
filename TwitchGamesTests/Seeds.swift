//
//  Seeds.swift
//  TwitchGamesTests
//
//  Created by Gustavo on 05/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

@testable import TwitchGames
import XCTest

struct Seeds
{
    struct Games
    {
        static let zelda_box = ImageReference.init(large: "https://static-cdn.jtvnw.net/ttv-boxart/The%20Legend%20of%20Zelda:%20Ocarina%20of%20Time-272x380.jpg",
                                                   medium: "https://static-cdn.jtvnw.net/ttv-boxart/The%20Legend%20of%20Zelda:%20Ocarina%20of%20Time-136x190.jpg",
                                                   small: "https://static-cdn.jtvnw.net/ttv-boxart/The%20Legend%20of%20Zelda:%20Ocarina%20of%20Time-52x72.jpg")
        
        static let tibia_box = ImageReference.init(large: "https://static-cdn.jtvnw.net/ttv-boxart/Tibia-272x380.jpg",
                                                   medium: "https://static-cdn.jtvnw.net/ttv-boxart/Tibia-136x190.jpg",
                                                   small: "https://static-cdn.jtvnw.net/ttv-boxart/Tibia-52x72.jpg")
        
        static let gta_box = ImageReference.init(large: "https://static-cdn.jtvnw.net/ttv-boxart/Grand%20Theft%20Auto:%20Vice%20City-272x380.jpg",
                                                   medium: "https://static-cdn.jtvnw.net/ttv-boxart/Grand%20Theft%20Auto:%20Vice%20City-136x190.jpg",
                                                   small: "https://static-cdn.jtvnw.net/ttv-boxart/Grand%20Theft%20Auto:%20Vice%20City-52x72.jpg")
        
        static let zelda = Game(name: "The Legend of Zelda: Ocarina of Time",
                                popularity: 2653,
                                _id: 11557,
                                locale: "",
                                box: zelda_box,
                                favorite: true,
                                viewers:100)
        
        static let tibia = Game(name: "Tibia",
                                popularity: 2346,
                                _id: 19619,
                                locale: "",
                                box: tibia_box,
                                favorite: true,
                                viewers:100)
        
        static let gta = Game(name: "Grand Theft Auto: Vice City",
                                popularity: 142,
                                _id: 15631,
                                locale: "",
                                box: gta_box,
                                favorite: true,
                                viewers:100)
    }
}
