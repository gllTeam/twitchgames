//
//  GameWorkerTests.swift
//  TwitchGamesTests
//
//  Created by Gustavo on 06/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

@testable import TwitchGames
import XCTest

class GameWorkerTests: XCTestCase
{
    // MARK: - Subject under test
    
    var sut: GameWorker!
    static var testGames: [Game]!
    
    // MARK: - Lifecycle
    
    override func setUp()
    {
        super.setUp()
        setupOrdersWorker()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    //MARK: - Setup
    
    func setupOrdersWorker()
    {
        sut = GameWorker.init(browserEngine: GameBrowserAPISpy(), favoriteEngine: GameFavoriteCoreDataSpy())
        
        GameWorkerTests.testGames = [Seeds.Games.zelda,Seeds.Games.tibia]
    }
    
    class GameBrowserAPISpy: GameBrowserAPI
    {
        
        var fetchTopGamesCalled:Bool = false
        var searchGameCalled:Bool = false
        
        override func fetchTopGames(offset: Int, completionHandler: @escaping FetchTopGamesCompletionHandler) {
            fetchTopGamesCalled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                completionHandler(GameBrowserResult.Success(result: GameWorkerTests.testGames))
            }
        }
        
        override func searchGame(query: String, completionHandler: @escaping SearchGameCompletionHandler) {
            searchGameCalled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                completionHandler(GameBrowserResult.Success(result: GameWorkerTests.testGames))
            }
        }
        
    }
    
    class GameFavoriteCoreDataSpy: GameFavoriteCoreData
    {
        
        var addGameToFavoriteCalled:Bool = false
        var fetchFavoriteGamesCalled:Bool = false
        var deleteGameFromeFavoriteCalled:Bool = false
        
        override func addGameToFavorite(game: Game, completionHandler: @escaping AddGameToFavoriteCompletionHandler) {
            addGameToFavoriteCalled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                if let game = GameWorkerTests.testGames.first{
                    completionHandler(GameFavoriteResult.Success(result: game))
                } else{
                    XCTFail("GameWorkerTests must init testGames")
                }
            }
        }
        
        override func fetchFavoriteGames(completionHandler: @escaping FetchFavoriteGamesCompletionHandler) {
            fetchFavoriteGamesCalled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                completionHandler(GameFavoriteResult.Success(result: GameWorkerTests.testGames))
            }
        }
        
        override func deleteGameFromFavorite(id: Int, completionHandler: @escaping DeleteGameFromFavoriteCompletionHandler) {
            deleteGameFromeFavoriteCalled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
                if let game = GameWorkerTests.testGames.first{
                    completionHandler(GameFavoriteResult.Success(result: game))
                } else{
                    XCTFail("GameWorkerTests must init testGames")
                }
            }
        }
        
    }
    
    //MARK: - Game Browser Spy Tests
    
    func testSearchGameShouldReturnListOfGames()
    {
        // Given
        let gameBrowserAPISpy = sut.browserGameEngine as! GameBrowserAPISpy
        
        // When
        var fetchedGames = [Game]()
        let expect = expectation(description: "Wait for searchGame() to return")
        sut.searchGame(query: "a") { (result) in
            switch result{
            case .Success(result: let games):
                fetchedGames = games
                break
            case .Failure(error: _):
                break
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 2)
        
        // Then
        XCTAssert(gameBrowserAPISpy.searchGameCalled, "Calling searchGame() should ask the data store for a list of games")
        XCTAssertEqual(fetchedGames.count, GameWorkerTests.testGames.count, "searchGame() should return a list of games")
        for game in fetchedGames {
            XCTAssert(GameWorkerTests.testGames.contains(game), "Search games should match the games in the data store")
        }
    }
    
    func testFetchTopGamesShouldReturnListOfGames()
    {
        // Given
        let gameBrowserAPISpy = sut.browserGameEngine as! GameBrowserAPISpy
        
        // When
        var fetchedGames = [Game]()
        let expect = expectation(description: "Wait for fetchTopGames() to return")
        sut.fetchTopGames(offset: 0) { (result) in
            switch result{
            case .Success(result: let games):
                fetchedGames = games
                break
            case .Failure(error: _):
                break
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 2)
        
        // Then
        XCTAssert(gameBrowserAPISpy.fetchTopGamesCalled, "Calling fetchTopGames() should ask the data store for a list of games")
        XCTAssertEqual(fetchedGames.count, GameWorkerTests.testGames.count, "fetchTopGames() should return a list of games")
        for game in fetchedGames {
            XCTAssert(GameWorkerTests.testGames.contains(game), "Fetched games should match the games in the data store")
        }
    }
    
    //MARK: - Game Favorite Spy Tests
    
    func testAddGameToFavoriteShouldReturnTheAddedGame()
    {
        // Given
        let gameFavoriteCoreDataSpy = sut.favoriteGameEngine as! GameFavoriteCoreDataSpy
        let gameToAdd = GameWorkerTests.testGames.first!
        
        // When
        var addedGame: Game?
        let expect = expectation(description: "Wait for addGameToFavorite() to return")
        sut.addGameToFavorite(game: gameToAdd) { (result) in
            switch result{
            case .Success(result: let game):
                addedGame = game
                break
            case .Failure(error: _):
                break
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 2)
        
        // Then
        XCTAssert(gameFavoriteCoreDataSpy.addGameToFavoriteCalled, "Calling addGameToFavorite() should ask the data store to add the new game")
        XCTAssertEqual(addedGame, gameToAdd, "addGameToFavorite() should add the new game")
    }
    
    func testFetchFavoriteGamesShouldReturnListOfGames()
    {
        // Given
        let gameFavoriteCoreDataSpy = sut.favoriteGameEngine as! GameFavoriteCoreDataSpy
        
        // When
        var fetchedGames = [Game]()
        let expect = expectation(description: "Wait for fetchFavoriteGames() to return")
        sut.fetchFavoriteGames { (result) in
            switch result{
            case .Success(result: let games):
                fetchedGames = games
                break
            case .Failure(error: _):
                break
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 2)
        
        // Then
        XCTAssert(gameFavoriteCoreDataSpy.fetchFavoriteGamesCalled, "Calling fetchFavoriteGames() should ask the data store for a list of games")
        XCTAssertEqual(fetchedGames.count, GameWorkerTests.testGames.count, "fetchFavoriteGames() should return a list of games")
        for game in fetchedGames {
            XCTAssert(GameWorkerTests.testGames.contains(game), "Fetched games should match the games in the data store")
        }
    }
    
    func testDeleteGameFromFavoriteShouldReturnTheAddedGame()
    {
        // Given
        let gameFavoriteCoreDataSpy = sut.favoriteGameEngine as! GameFavoriteCoreDataSpy
        let gameToDelete = GameWorkerTests.testGames.first!
        
        // When
        var deletedGame: Game?
        let expect = expectation(description: "Wait for deleteGameFromeFavorite() to return")
        sut.deleteGameFromFavorite(id: gameToDelete._id!) { (result) in
            switch result{
            case .Success(result: let game):
                deletedGame = game
                break
            case .Failure(error: _):
                break
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 2)
        
        // Then
        XCTAssert(gameFavoriteCoreDataSpy.deleteGameFromeFavoriteCalled, "Calling deleteGameFromeFavorite() should ask the data store to delete the game")
        XCTAssertEqual(deletedGame, gameToDelete, "deleteGameFromeFavorite() should delete the game")
    }
    
}
