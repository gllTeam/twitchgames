//
//  GameFavoriteViewModelTests.swift
//  TwitchGamesTests
//
//  Created by Gustavo Luís Soré on 10/06/2018.
//  Copyright © 2018 Sore. All rights reserved.
//

@testable import TwitchGames
import XCTest

class GameFavoriteViewModelTests: XCTestCase
{
    
    // MARK: - Subject under test
    
    var sut: GameFavoriteViewModel!
    
    // MARK: - Lifecycle
    
    override func setUp()
    {
        super.setUp()
        setupGameBrowserViewModel()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    //MARK: - Setup
    
    func setupGameBrowserViewModel()
    {
        
        sut = GameFavoriteViewModel.init(worker: GameWorkerSpy(browserEngine: GameBrowserAPI(), favoriteEngine: GameFavoriteCoreData()))
        
    }
    
    class GameWorkerSpy: GameWorker
    {
        
        var fetchFavoriteGamesCalled:Bool = false
        var deleteGameFromFavoriteCalled:Bool = false
        
        override func fetchFavoriteGames(completionHandler: @escaping FetchFavoriteGamesCompletionHandler) {
            fetchFavoriteGamesCalled = true
        }
        
        override func deleteGameFromFavorite(id: Int, completionHandler: @escaping DeleteGameFromFavoriteCompletionHandler) {
            deleteGameFromFavoriteCalled = true
        }
    }
    
    //MARK: - Tests
    
    func testFetchFavoriteGamesShouldCallWorker()
    {
        
        // Given
        let gameWorkerSpy = sut.gameWork as! GameWorkerSpy
        
        // When
        sut.getFavoriteGames()
        sut.getFavoriteGames()
        
        //Then
        XCTAssert(gameWorkerSpy.fetchFavoriteGamesCalled, "Calling getFavoriteGames() should call fetchFavoriteGames() from worker")
        
    }
    
    func testSearchGameShouldCallWorker()
    {
        
        // Given
        let gameWorkerSpy = sut.gameWork as! GameWorkerSpy
        
        // When
        sut.deleteGameFromFavorites(game: Seeds.Games.gta,completionHandle: {
            
        })
        
        //Then
        XCTAssert(gameWorkerSpy.deleteGameFromFavoriteCalled, "Calling deleteGameFromFavorites() should call deleteGameFromFavorite() from worker")
        
    }
    
}
