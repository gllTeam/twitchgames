//
//  GameBrowserViewModelTests.swift
//  TwitchGamesTests
//
//  Created by Gustavo on 06/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

@testable import TwitchGames
import XCTest

class GameBrowserViewModelTests: XCTestCase
{
    
    // MARK: - Subject under test
    
    var sut: GameBrowserViewModel!
    var testNumberOfGamesPerRequest = 2
    
    // MARK: - Lifecycle
    
    override func setUp()
    {
        super.setUp()
        setupGameBrowserViewModel()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    //MARK: - Setup
    
    func setupGameBrowserViewModel()
    {

        sut = GameBrowserViewModel.init(worker: GameWorkerSpy(browserEngine: GameBrowserAPI(), favoriteEngine: GameFavoriteCoreData()), numberOfGamesPerRequest: testNumberOfGamesPerRequest)
        
    }
    
    class GameWorkerSpy: GameWorker
    {
        
        var fetchTopGamesCalled:Bool = false
        var searchGameCalled:Bool = false
        
        override func fetchTopGames(offset: Int, completionHandler: @escaping FetchTopGamesCompletionHandler) {
            fetchTopGamesCalled = true
        }
        
        override func searchGame(query: String, completionHandler: @escaping SearchGameCompletionHandler) {
            searchGameCalled = true
        }
    }
    
    //MARK: - Tests
    
    func testGetTopGamesShouldCallWorker()
    {
        
        // Given
        let gameWorkerSpy = sut.gameWork as! GameWorkerSpy
        
        // When
        sut.getTopGames()
        
        //Then
        XCTAssert(gameWorkerSpy.fetchTopGamesCalled, "Calling getTopGames() should call fetchTopGames() from worker")
        
    }
    
    func testSearchGameShouldCallWorker()
    {
        
        // Given
        let gameWorkerSpy = sut.gameWork as! GameWorkerSpy
        
        // When
        sut.searchGame(query: "a")
        
        //Then
        XCTAssert(gameWorkerSpy.searchGameCalled, "Calling searchGame() should call searchGame() from worker")
        
    }
    
}
