//
//  MessageImageView.swift
//  TwitchGameMessage MessagesExtension
//
//  Created by Gustavo on 11/06/18.
//  Copyright © 2018 Sore. All rights reserved.
//

import UIKit

class MessageImageView: UIImageView {

    var game:Game?
    
    var actionCompletionHandle:((_ game: Game) -> ())?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let game = self.game{
            actionCompletionHandle?(game)
        }
    }

}
